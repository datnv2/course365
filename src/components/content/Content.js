import React from "react";

export const Content = () => {
  return (
    <section className="home">
      <div className="home_title">
        <h1>Welcome to Geeks UI Learning Application</h1>
        <p>
          Hand-picked Instructor and expertly crafted courses, designed for the
          modern students and entrepreneur.
        </p>
        <div className="home_title_btn">
          <button className="btn-btn_browse">browser courses</button>
          <button className="btn-btn_instructor">are you instructor ?</button>
        </div>
      </div>
      <div className="home_image">
        <img src="https://codescandy.com/geeks-bootstrap-5/assets/images/hero/hero-img.png" />
      </div>
    </section>
  );
};

export default Content;
