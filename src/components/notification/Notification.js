import React from "react";

export const Notification = () => {
  return (
    <div className="notification">
      <div className="notification-item online-courses">
        <span className="fa-video-circel">
          <i className="icon-video fas fa-video"></i>
        </span>
        <div className="text-online-courses">
          <div className="courses-text">30,000 online courses</div>
          <p className="enjoy-text ">Enjoy a variety of fresh topics</p>
        </div>
      </div>
      <div className="notification-item expert-instruction">
        <span className="user-friends-circel">
          <i className="icon-friends fas fa-user-friends"></i>
        </span>
        <div className="right-instructor">
          <span className="instruction-text">Expert instruction</span>
          <p className="find-text">Find the right instructor for you</p>
        </div>
      </div>
      <div className="notification-item lifetime-access">
        <span className="fa-clock-circel">
          <i className="icon-clock far fa-clock"></i>
        </span>
        <div className="schedule-access">
          <span className="access-text">Lifetime access</span>
          <p className="schedule-text">Learn on your schedule</p>
        </div>
      </div>
    </div>
  );
};

export default Notification;
