import React, { useState, useEffect } from "react";
import axios from "axios";
import { Link, useHistory } from "react-router-dom";
import { signInWithGoogle } from "../../Firebase/Config.js";

export const Login = () => {
  const history = useHistory();
  const [spanErr, setSpanError] = useState(false);
  const [email, setEmail] = useState("");

  //hàm này thực hiện việc đăng nhập
  const loginAccountClick = async () => {
    if (email !== "") {
      const loginUser = await axios.get(
        "http://localhost:8000/customer-login?login=" + email
      );
      if (loginUser.data.customer !== null) {
        const name = loginUser.data.customer.fullName;
        const emailLogin = loginUser.data.customer.email;
        const profilePic = loginUser.data.customer.photoURL;
        const address = loginUser.data.customer.address;
        const phone = loginUser.data.customer.phone;
        const id = loginUser.data.customer._id;
        localStorage.setItem("name", name);
        localStorage.setItem("email", emailLogin);
        localStorage.setItem("profilePic", profilePic);
        localStorage.setItem("address", address);
        localStorage.setItem("phone", phone);
        localStorage.setItem("id", id);
        history.push("/");
      } else {
        setSpanError(true);
      }
    } else {
      setSpanError(true);
    }
  };
  return (
    <div className="login-container">
      <div className="login-name">
        <h2 className="login-name-account">Login Account</h2>
      </div>
      <div className="login-form">
        <h3 className="login-text">Đăng nhập</h3>
        <p className="login-email-phone">
          Đăng nhập bằng email hoặc số điện thoại
        </p>
        <input
          className="login-input"
          placeholder="  nhập email hoặc số điện thoại ..."
          onChange={(e) => setEmail(e.target.value)}
        />
        <span className={spanErr ? "span-err" : "span-hide"}>
          email hoặc số điện thoại không tồn tại. Vui lòng đăng ký tài khoản
        </span>
        <button className="login-btn" onClick={loginAccountClick}>
          Đăng nhập
        </button>
        <div className="login-google">
          <button className="login-btn-google" onClick={signInWithGoogle}>
            <i className="icon-google fab fa-google"></i>
            Đăng nhập với google
          </button>
        </div>
      </div>
      <div className="create-new-account">
        <Link to="/create-account">
          <button className="create-btn-account">Tạo tài khoản</button>
        </Link>
      </div>
      <div className="come-back">
        <Link to="/">
          <button className="come-back-btn">Quay lại</button>
        </Link>
      </div>
    </div>
  );
};
export default Login;
