import React, { useState, useEffect } from "react";
import axios from "axios";
import { useHistory } from "react-router-dom";
import Header from "../header/Header.js";
import Footer from "../footer/Footer.js";
import {
  Container,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  Grid,
} from "@mui/material";

import ModalOrderDetail from "./ModalOrderDetail/ModalOrderDetail.js";

export const MyOrder = () => {
  const history = useHistory();
  // set list order của khách hàng là 1 mảng rỗng để gán lại giá trị sau khi gọi api
  const [myOrder, setMyOrder] = useState([]);

  // sét trạng thái để đóng mở modal show các order của khách hàng lên
  const [show, setShow] = useState(false);

  // hàm này thực hiện đóng modal lại
  const handleCloseModal = () => {
    setShow(false);
  };

  // hàm này thực hiện việc mở modal lên
  const handleShowModal = () => {
    setShow(true);
  };

  // gán cho chi tiết order của user là 1 mảng rỗng
  const [orderDetail, setOrderDetail] = useState([]);

  // thực hiện gọi api để lấy dánh sách đơn hàng theo thông tin của user trên localStorage
  useEffect(() => {
    const loadAllOrderByCustomerId = async () => {
      const orderDetailUser = await axios.get(
        "http://localhost:8000/order/" + localStorage.getItem("id")
      );
      if (orderDetailUser.data.Order !== null) {
        setMyOrder(orderDetailUser.data.Order);
      }
    };
    loadAllOrderByCustomerId();
  }, []);

  // thực hiện gọi api để lấy ra chi tiết của từng đơn hàng theo id
  const handlerDetailOrder = async (Order) => {
    const orderDetailId = await axios.get(
      "http://localhost:8000/order-detail-id/" + Order._id
    );
    setOrderDetail(orderDetailId.data.orderDetail);
    handleShowModal();
  };

  // hàm này thực hiện quay lại trang chủ
  const backToHome = () => {
    history.push("/");
  };
  return (
    <div>
      <Header />
      <div className="order-container">
        <h2 className="order-list">LIST ORDER</h2>
        <div className="order-table">
          <Grid container>
            <TableContainer
              component={Paper}
              style={{ border: "1px solid #f7e7ce" }}
            >
              <Table sx={{ minWidth: 650 }} aria-label="simple table">
                <TableHead>
                  <TableRow style={{ border: "1px solid #f7e7ce" }}>
                    <TableCell
                      align="center"
                      style={{ border: "1px solid #f7e7ce" }}
                    >
                      STT
                    </TableCell>
                    <TableCell
                      align="center"
                      style={{ border: "1px solid #f7e7ce" }}
                    >
                      Order Date
                    </TableCell>
                    <TableCell
                      align="center"
                      style={{ border: "1px solid #f7e7ce" }}
                    >
                      Required Date
                    </TableCell>

                    <TableCell
                      align="center"
                      style={{ border: "1px solid #f7e7ce" }}
                    >
                      Note
                    </TableCell>
                    <TableCell
                      align="center"
                      style={{ border: "1px solid #f7e7ce" }}
                    >
                      Status
                    </TableCell>
                    <TableCell
                      align="center"
                      style={{ border: "1px solid #f7e7ce" }}
                    >
                      Action
                    </TableCell>
                  </TableRow>
                </TableHead>
                <TableBody style={{ border: "1px solid #f7e7ce" }}>
                  {myOrder.map((element, index) => (
                    <TableRow
                      key={index}
                      sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                      style={{ border: "1px solid #f7e7ce" }}
                    >
                      <TableCell
                        component="th"
                        scope="row"
                        align="center"
                        style={{ border: "1px solid #f7e7ce" }}
                      >
                        {" "}
                        {index + 1}
                      </TableCell>
                      <TableCell
                        align="center"
                        style={{ border: "1px solid #f7e7ce" }}
                      >
                        {" "}
                        {element.orderDate?.slice(0, 10)}
                      </TableCell>
                      <TableCell
                        style={{ border: "1px solid #f7e7ce" }}
                        align="center"
                      >
                        {element.requiredDate?.slice(0, 10)}
                      </TableCell>
                      <TableCell
                        style={{ border: "1px solid #f7e7ce" }}
                        align="center"
                      >
                        {element.note?.slice(0, 10)}
                      </TableCell>
                      {/* <TableCell
                      style={{ border: "1px solid #f7e7ce" }}
                      align="center"
                    ></TableCell> */}
                      <TableCell
                        style={{ border: "1px solid #f7e7ce" }}
                        align="center"
                      >
                        {" "}
                        {element.status}
                      </TableCell>
                      <TableCell
                        align="center"
                        style={{ border: "1px solid #f7e7ce" }}
                      >
                        <button className="order-back" onClick={backToHome}>
                          Quay lại
                        </button>
                        <button
                          className="order-list-detail"
                          onClick={() => handlerDetailOrder(element)}
                        >
                          {" "}
                          Chi tiết
                        </button>
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
          </Grid>
        </div>
      </div>
      <div className="footer-top-distance">
        <Footer />
      </div>
      <ModalOrderDetail
        show={show}
        orderDetail={orderDetail}
        handlerClose={handleCloseModal}
        backHome={backToHome}
      />
    </div>
  );
};

export default MyOrder;
