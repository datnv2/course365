import React, { useState } from "react";
import axios from "axios";
import { useHistory } from "react-router-dom";
import { Modal } from "react-bootstrap";

export const CustomerInfo = () => {
  const history = useHistory();
  // khai báo trạng thái báo lỗi trên input
  const [errMassage, setErrorMessage] = useState("");
  // sét trạng thái đóng mở modal
  const [show, setShow] = useState(false);

  // hàm thực hiện đóng modal lại
  const handlerCloseModal = () => {
    setShow(false);
  };

  //hàm thực hiện mở modal lên
  const handlerOpenModal = () => {
    setShow(true);
  };

  // phần thông tin của user là 1 đối tượng và lấy data từ localStorage
  const [profile, setProfile] = useState({
    fullName: localStorage.name,
    email: localStorage.email,
    phone: localStorage.phone,
    adress: localStorage.address,
  });

  // hàm thực hiện việc khi user không update nữa bấm cancel thì quay về trang chủ
  const canUpdate = () => {
    history.push("/");
  };

  // hàm thực hiện update thông tin user
  // const updateDataUser = async () => {
  //   if (
  //     profile.phone === localStorage.phone &&
  //     profile.email === localStorage.email
  //   ) {
  //     const update = await axios.put(
  //       "http://localhost:8000/customer/update/" +
  //         profile.phone +
  //         "" +
  //         profile.email,
  //       profile
  //     );
  //     if (update.data.success) {
  //       localStorage.removeItem("name");
  //       localStorage.removeItem("email");
  //       localStorage.removeItem("phone");
  //       localStorage.removeItem("adress");
  //       localStorage.setItem("name", update.data.customer.fullName);
  //       localStorage.setItem("email", update.data.customer.email);
  //       localStorage.setItem("phone", update.data.customer.phone);
  //       localStorage.setItem("adress", update.data.customer.adress);
  //       setErrorMessage("cập nhật thành công");
  //       handlerOpenModal();
  //     }
  //   } else if (profile.email === localStorage.email) {
  //     const checkPhone = await axios.get(
  //       "http://localhost:8000/customer-phone?phone=" + profile.phone
  //     );
  //     if (checkPhone.data.customer === null) {
  //       const updateEmail = await axios.put(
  //         "http://localhost:8000/customer/update-email?email=" + profile.email,
  //         profile
  //       );
  //       localStorage.removeItem("name");
  //       localStorage.removeItem("email");
  //       localStorage.removeItem("phone");
  //       localStorage.removeItem("adress");
  //       localStorage.setItem("name", updateEmail.data.customer.fullName);
  //       localStorage.setItem("email", updateEmail.data.customer.email);
  //       localStorage.setItem("phone", updateEmail.data.customer.phone);
  //       localStorage.setItem("adress", updateEmail.data.customer.adress);
  //       setErrorMessage("cập nhật thành công");
  //       handlerOpenModal();
  //     } else {
  //       setErrorMessage("cập nhật thất bại số điện thoại đã trùng");
  //       handlerOpenModal();
  //     }
  //   } else if (profile.phone === localStorage.phone) {
  //     const checkEmail = await axios.get(
  //       "http://localhost:8000/customer-email?email=" + profile.email
  //     );
  //     if (checkEmail.data.customer === null) {
  //       const updatePhone = await axios.put(
  //         "http://localhost:8000/customer/update-phone?phone=" + profile.phone,
  //         profile
  //       );
  //       localStorage.removeItem("name");
  //       localStorage.removeItem("email");
  //       localStorage.removeItem("phone");
  //       localStorage.removeItem("adress");
  //       localStorage.setItem("name", updatePhone.data.customer.fullName);
  //       localStorage.setItem("email", updatePhone.data.customer.email);
  //       localStorage.setItem("phone", updatePhone.data.customer.phone);
  //       localStorage.setItem("adress", updatePhone.data.customer.adress);
  //       setErrorMessage("cập nhật thành công");
  //       handlerOpenModal();
  //     } else {
  //       setErrorMessage("cập nhật thất bại email đã tồn tại");
  //       handlerOpenModal();
  //     }
  //   } else {
  //     const CheckUser = await axios.get(
  //       "http://localhost:8000/customer-email-phone?email=" +
  //         profile.email +
  //         "&phone=" +
  //         profile.phone
  //     );
  //     if (CheckUser.data.customer === null) {
  //       const updateUser = await axios.put(
  //         "http://localhost:8000/customer-update-email-or-phone?email=" +
  //           profile.email,
  //         profile
  //       );
  //       localStorage.removeItem("name");
  //       localStorage.removeItem("email");
  //       localStorage.removeItem("phone");
  //       localStorage.removeItem("adress");
  //       localStorage.setItem("name", updateUser.data.customer.fullName);
  //       localStorage.setItem("email", updateUser.data.customer.email);
  //       localStorage.setItem("phone", updateUser.data.customer.phone);
  //       localStorage.setItem("adress", updateUser.data.customer.adress);
  //       setErrorMessage("cập nhật thành công");
  //       handlerOpenModal();
  //     }
  //   }
  // };

  //hàm thực hiện sau khi bấm đóng modal update thì quay về trang chủ
  const handleUpdateClick = async () => {
    // nếu profile user update mà trùng thông tin với data có trên localStorage thì tiến hành gọi api để update thông tin user
    if (
      profile.email === localStorage.email &&
      profile.phone === localStorage.phone
    ) {
      const update = await axios.put(
        "http://localhost:8000/customer/update/" +
          profile.email +
          "/" +
          profile.phone,
        profile
      );
      // nếu update thành công thì xóa thông tin cũ và update lại thông tin mới cho user
      if (update.data.success) {
        localStorage.removeItem("name");
        localStorage.removeItem("email");
        localStorage.removeItem("phone");
        localStorage.removeItem("address");

        localStorage.setItem("name", update.data.customer.fullName);
        localStorage.setItem("email", update.data.customer.email);
        localStorage.setItem("phone", update.data.customer.phone);
        localStorage.setItem("address", update.data.customer.address);

        setErrorMessage("cập nhật thành công");
        handlerOpenModal();
      }
      //còn nếu email trong thông tin của user trùng với email trên localStorage thì lấy thông tin ra
    } else if (profile.email === localStorage.email) {
      const checkPhone = await axios.get(
        "http://localhost:8000/customer-phone?phone=" + profile.phone
      );
      // nếu data user kiểm tra theo số điện thoai hoặc email mà null thì gọi api để user cập nhật thông tin theo pfofile
      if (checkPhone.data.customer === null) {
        const updateEmail = await axios.put(
          "http://localhost:8000/customer/update-email?email=" + profile.email,
          profile
        );
        localStorage.removeItem("name");
        localStorage.removeItem("email");
        localStorage.removeItem("phone");
        localStorage.removeItem("address");

        localStorage.setItem("name", updateEmail.data.customer.fullName);
        localStorage.setItem("email", updateEmail.data.customer.email);
        localStorage.setItem("phone", updateEmail.data.customer.phone);
        localStorage.setItem("address", updateEmail.data.customer.address);

        setErrorMessage("cập nhật thành công");
        handlerOpenModal();
        //còn nếu email hoặc số đt cập nhật trùng với data đã tồn tại thì báo cập nhật thất bại
      } else {
        setErrorMessage("cập nhật thất bại, số điện thoại đã tồn tại");
        handlerOpenModal();
      }
      //nếu phone trùng với phone trên localStorage thì lấy ra thông tin user
    } else if (profile.phone === localStorage.phone) {
      const checkEmail = await axios.get(
        "http://localhost:8000/customer-email?email=" + profile.email
      );
      //nếu check data mà null thì gọi api để user update thông tin và set lại data trên localStorage
      if (checkEmail.data.customer === null) {
        const updateEmail = await axios.put(
          "http://localhost:8000/customer/update-phone?phone=" + profile.phone,
          profile
        );
        localStorage.removeItem("name");
        localStorage.removeItem("email");
        localStorage.removeItem("phone");
        localStorage.removeItem("address");

        localStorage.setItem("name", updateEmail.data.customer.fullName);
        localStorage.setItem("email", updateEmail.data.customer.email);
        localStorage.setItem("phone", updateEmail.data.customer.phone);
        localStorage.setItem("address", updateEmail.data.customer.address);

        setErrorMessage("cập nhật thành công");
        handlerOpenModal();
      } else {
        setErrorMessage("cập nhật thất bại email đã tồn tại");

        handlerOpenModal();
      }
    } else {
      const checkCustomer = await axios.get(
        "http://localhost:8000/customer-email-phone?email=" +
          profile.email +
          "&phone=" +
          profile.phone
      );
      if (checkCustomer.data.customer === null) {
        const updateCustomer = await axios.put(
          "http://localhost:8000/customer-update-email-or-phone?email=" +
            localStorage.email,
          profile
        );
        localStorage.removeItem("name");
        localStorage.removeItem("email");
        localStorage.removeItem("phone");
        localStorage.removeItem("address");

        localStorage.setItem("name", updateCustomer.data.customer.fullName);
        localStorage.setItem("email", updateCustomer.data.customer.email);
        localStorage.setItem("phone", updateCustomer.data.customer.phone);
        localStorage.setItem("address", updateCustomer.data.customer.address);

        handlerOpenModal();

        setErrorMessage("cập nhật thành công");
      }
    }
  };

  const handleSave = () => {
    handlerOpenModal();
    history.goBack();
  };
  return (
    <div className="customer-container">
      <div className="customer-container-header">
        <h3 className="customer-container-header--text">
          Thông tin người dùng
        </h3>
      </div>
      <div className="customer-content">
        <div className="input-container">
          <input
            className="input-controler"
            placeholder="  "
            defaultValue={profile.fullName}
            onChange={(e) =>
              setProfile({ ...profile, fullName: e.target.value })
            }
          />
          <label className="form-label--name">Họ tên</label>
        </div>
        <div className="input-container">
          <input
            className="input-controler"
            placeholder="  "
            defaultValue={profile.email}
            onChange={(e) => setProfile({ ...profile, email: e.target.value })}
          />
          <label className="form-label--name">Email</label>
        </div>
        <div className="input-container">
          <input
            className="input-controler"
            placeholder="  "
            defaultValue={profile.phone}
            onChange={(e) => setProfile({ ...profile, phone: e.target.value })}
          />
          <label className="form-label--name">Số điện thoại</label>
        </div>
        <div className="input-container">
          <input
            className="input-controler"
            placeholder="  "
            defaultValue={profile.adress}
            onChange={(e) => setProfile({ ...profile, phone: e.target.value })}
          />
          <label className="form-label--name">Địa chỉ</label>
        </div>
        <div className="customer-bottom">
          <button className="btn-cancel" onClick={canUpdate}>
            Quay lại
          </button>
          <button className="btn-update-user" onClick={handleUpdateClick}>
            Cập nhật
          </button>
        </div>
      </div>
      <Modal show={show} onHide={handlerCloseModal}>
        <Modal.Header closeButton>
          <Modal.Title>Thông báo</Modal.Title>
        </Modal.Header>
        <Modal.Body>{errMassage}</Modal.Body>
        <Modal.Footer>
          <button className="btn-cancel" onClick={handleSave}>
            Quay lại
          </button>
          <button className="btn-update-user" onClick={handlerCloseModal}>
            Đóng
          </button>
        </Modal.Footer>
      </Modal>
    </div>
  );
};

export default CustomerInfo;
