import React, { useState, useEffect } from "react";
import axios from "axios";
import { Link, useParams } from "react-router-dom";

export const CourseList = () => {
  const { key, name } = useParams();
  // khai báo mảng course ban đầu là rỗng để gán lại giá trị sau khi gọi api
  const [course, setCourse] = useState([]);

  // thực hiện gọi api để lấy course về
  useEffect(() => {
    const allCourse = async () => {
      try {
        const listCourse = await axios.get("http://localhost:8000/products");
        //console.log(listCourse);
        setCourse(listCourse.data.Product);

        // thực hiện tìm kiếm
        if (key === "all" && name === "all") {
          let filterDb = listCourse.data.Product;
          setCourse(filterDb);
          return;
        }

        if (key === "name" || key === "all") {
          let filterDb = listCourse.data.Product.filter((data) => {
            return data.productName.toLowerCase().includes(name.toLowerCase());
          });
          setCourse(filterDb);
          return;
        }
      } catch (e) {
        console.log(e.message);
      }
    };
    allCourse();
  }, [key, name]);

  // đường linh để dẫn đến trang detail course
  const detailUrl = "/detail/";
  return (
    <div className="course-container">
      <div className="grid">
        <h2>LIST COURSE</h2>
        <div className="grid__row">
          {course.map((element, index) => (
            <div className="gird__colum-2-4" key={index}>
              <a className="home-product-item">
                {/* ảnh sản phẩm */}
                <div className="home-product-avarta">
                  <Link className="card-link" to={detailUrl + element._id}>
                    <img
                      src={element.productImage}
                      className="home-product-item__img"
                    />
                  </Link>
                </div>
                {/* tên sản phẩm */}
                <Link className="card-link" to={detailUrl + element._id}>
                  <h4 className="title-name">{element.productName}</h4>
                </Link>
                {/* thời gian học và level */}
                <div className="time-to-achieve-level">
                  <span className="icon-clock-achieve far fa-clock" />
                  <span className="time-spent">{element.studyTime}</span>
                  <span className="level-achieve">{element.level}</span>
                </div>
                <div className="line-separate"></div>
                {/* footer */}
                <div className="footer-item">
                  <span className="footer-item-name-teacher">
                    <img src={element.teacherAvarta} className="img-avarta" />
                  </span>
                  <span className="teacher-name">{element.teacherName}</span>
                </div>
              </a>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default CourseList;
