import React, { useState } from "react";
import axios from "axios";
import { Modal } from "react-bootstrap";
import { useHistory } from "react-router-dom";

export const CreateAccount = () => {
  const history = useHistory();
  // lấy thông tin từ fullName localStorage
  const [fullName, setFullName] = useState(localStorage.name);
  // lấy thông tin từ email localStorage
  const [email, setEmail] = useState(localStorage.email);
  const [phone, setPhone] = useState("");
  const [address, setAddress] = useState("");
  // sét trạng thái cho modal khi tạo thành công
  const [show, setShow] = useState(false);

  //  hàm thực hiện đóng modal
  const handlerClose = () => {
    setShow(false);
  };

  // set trạng thái báo lỗi khi điện thoại để trống
  const [phoneError, setPhoneError] = useState(false);
  // set trạng thái báo lỗi khi địa chỉ để trống
  const [addressErr, setAddressErr] = useState(false);

  // set trạng thái check để thông báo tạo user thành công hay thất bại
  const [modalCheck, setModalCheck] = useState(false);

  // hàm để check xem user đã có trên database chưa nếu thỏa mãn thì mới gọi api để tạo tài khoản mới
  // const checkPhoneEmailUserCreate = async () => {
  //   if (phone == !"" || email == !"") {
  //     const checkPhoneUser = await axios.get(
  //       "http://localhost:8000/customer-phone?phone=" + phone
  //     );
  //     const checkEmailUser = await axios.get(
  //       "http://localhost:8000/customer-email?email=" + email
  //     );
  //     if (
  //       checkPhoneUser.data.customer !== null &&
  //       checkEmailUser.data.customer !== null
  //     ) {
  //       const customer = {
  //         fullName: fullName,
  //         email: email,
  //         phone: phone,
  //         address: address,
  //       };
  //       const create = await axios.get(
  //         "http://localhost:8000/customer",
  //         customer
  //       );
  //       setModalCheck(true);
  //       if (create.data.customer !== null) {
  //         setShow(true);
  //       }
  //     } else {
  //       setShow(true);
  //     }
  //   } else {
  //     setPhoneError(true);
  //     setAddressErr(true);
  //   }
  // };

  // hàm này gọi api để người dùng tạo mới tài khoản và check xem email hay phone đã có trong database chưa
  const checkPhoneEmailAndCreateUserAccount = async () => {
    //nếu mà phone hoặc email mà đã có thì gọi api để ktra trong database
    if (phone !== "" && address !== "") {
      const checkPhone = await axios.get(
        "http://localhost:8000/customer-phone?phone=" + phone
      );
      const checkEmail = await axios.get(
        "http://localhost:8000/customer-email?email=" + email
      );
      // nếu kiểm tra mà chưa có email và số điện thoại trong database thì gọi api để tạo mới cho người dùng
      if (
        checkPhone.data.customer === null &&
        checkEmail.data.customer === null
      ) {
        const customer = {
          fullName: fullName,
          phone: phone,
          email: email,
          address: address,
        };

        // gọi api để tạo tk
        const create = await axios.post(
          "http://localhost:8000/customer",
          customer
        );
        setModalCheck(true);

        // nếu tạo mới user thành công thì mở modal thông báo lên
        if (create.data.customer !== null) {
          setShow(true);
        }
      } else {
        setShow(true);
      }
    } else {
      setPhoneError(true);
      setAddressErr(true);
    }
  };

  const backToHome = () => {
    history.push("/");
  };
  return (
    <div className="container-create">
      <div className="title-name-create">
        <h1>Tạo Tài Khoản</h1>
      </div>
      <div className="form-create-account">
        <label>Họ tên</label>
        <input
          type="text"
          defaultValue={fullName}
          onChange={(e) => setFullName(e.target.value)}
        />
        <label>Email</label>
        <input
          type="text"
          defaultValue={email}
          onChange={(e) => setEmail(e.target.value)}
        />
        <span className={phoneError ? "show-err" : "hide-err"}>
          email không được để trống
        </span>
        <label>Số điện thoại</label>
        <input
          type="text"
          defaultValue={phone}
          onChange={(e) => setPhone(e.target.value)}
        />
        <span className={phoneError ? "show-err" : "hide-err"}>
          số điện thoại không được để trống
        </span>
        <label>Địa chỉ</label>
        <input
          type="text"
          defaultValue={address}
          onChange={(e) => setAddress(e.target.value)}
        />
        <span className={addressErr ? "show-err" : "hide-err"}>
          địa chỉ không được để trống
        </span>
        <button
          className="btn-create"
          onClick={checkPhoneEmailAndCreateUserAccount}
        >
          Tạo tài khoản
        </button>
        <button className="btn-back" onClick={backToHome}>
          Quay lại
        </button>
      </div>
      <Modal show={show} onHide={handlerClose} animation={true}>
        <Modal.Header closeButton>
          <Modal.Title>Thông báo</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {modalCheck
            ? "Đã tạo tài khoản thành công"
            : "Tạo tài khoản thất bại"}
        </Modal.Body>
        <Modal.Footer>
          <button className="btn-update" onClick={handlerClose}>
            Đóng
          </button>
        </Modal.Footer>
      </Modal>
    </div>
  );
};

export default CreateAccount;
