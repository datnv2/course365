import React, { useState, useEffect } from "react";
import axios from "axios";
import { useHistory } from "react-router-dom";
import CourseList from "./CourseList.js";
import { useDispatch } from "react-redux";
import { addProduct } from "../../actions/cart.js";

export const DetailCoures = (props) => {
  // const { id } = props.match.params;
  // const [quantity, setQuantity] = useState(1);
  // const [product, setProduct] = useState([]);
  // const [imgIcon, setImg] = useState([]);
  // const [iconIndex, setIconIndex] = useState(0);
  // const history = useHistory();
  // const dispatch = useDispatch();

  const { id } = props.match.params;
  const history = useHistory();
  const dispatch = useDispatch();
  const [product, setProduct] = useState([]);
  const [imgIcon, setImg] = useState([]);
  const [iconIndex, setIconIndex] = useState(0);
  const [quantity, setQuantity] = useState(1);

  const randomNumber = () => {
    return 1000 + Math.trunc(Math.random() * 9000);
  };

  // gọi api để load thông tin của sản phẩm theo id
  useEffect(() => {
    const getProductByProductId = async () => {
      try {
        const productDB = await axios.get(
          "http://localhost:8000/products/" + id
        );
        setProduct(productDB.data.Product);
        setImg(productDB.data.Product.productImage);
        // let scrollElement = document.getElementById("scroll");
        // scrollElement.scrollIntoView();
      } catch (err) {
        console.log(err.message);
      }
    };
    getProductByProductId();
  }, [id]);

  // hàm này để thay đổi ảnh của sản phẩm theo id được click trong detail của sản phẩm
  const onChangeIcon = (e) => {
    let iconIndex = e.target.getAttribute("data-key");
    setIconIndex(iconIndex);
  };

  //   khai báo ngày tháng
  let today = new Date();
  let date =
    today.getDate() +
    "/" +
    parseInt(today.getMonth() + 1) +
    "/" +
    today.getFullYear();

  // hàm thực hiện quay lại trang chủ
  const backToHome = () => {
    history.push("/");
  };

  // hàm  thực hiện việc thêm sản phẩm vào giỏ hàng
  const handleBtnBuyClick = () => {
    if (localStorage.name === null || localStorage.name === undefined) {
      history.push("/login");
    } else {
      let productDetail = product;
      productDetail.productQuantity = quantity;
      const productCart = {
        idPro: randomNumber(),
        product: productDetail,
      };
      const action = addProduct(productCart);
      dispatch(action);
    }
  };

  return (
    <div>
      <div className="detail-container">
        {/* ảnh sản phẩm */}
        <div className="detail-img-item-course">
          <h2 className="course-text">CHI TIẾT KHÓA HỌC</h2>
          <div className="detail-img">
            {imgIcon ? (
              <img
                alt=""
                src={imgIcon[iconIndex]}
                className="img-detail-course"
              />
            ) : (
              ""
            )}
          </div>
          <div className="detail-content-image-list-icon">
            {imgIcon
              ? imgIcon.map((element, index) => (
                  <img
                    key={index}
                    data-key={index}
                    alt=""
                    src={element}
                    onClick={(index) => {
                      onChangeIcon(index);
                    }}
                  />
                ))
              : ""}
          </div>
        </div>
        {/* thông tin sản phẩm */}
        <div className="detail-course">
          <p className="detail-course-name">
            <span className="detail-course-name-product">
              Tên khóa học:&nbsp;{" "}
            </span>
            {product.productName}
          </p>
          <p className="detail-course-original-price">
            <span>Giá Gốc: {product.priceSale} $</span>
          </p>
          <p className="detail-course-price-sale">
            <span>Giá Khuyến mại: {product.productPrice} $</span>
          </p>
          <p className="detail-course-info">
            <span>Chi tiết khóa học: {product.productDetail} </span>
          </p>
          <p className="detail-course-teacher">
            <span>Giáo viên: {product.teacherName} </span>
          </p>
          <p className="detail-course-time-to-learn">
            <span>Thời gian học: {product.studyTime} </span>
          </p>
          <p className="detail-course-level">
            <span>Tiêu chuẩn: {product.level} </span>
          </p>
        </div>
        {/* ngày order */}
        <div className="add-course-to-cart">
          <div className="time-order">
            <span>Today: {date}</span>
          </div>
          <div className="time-separate"></div>
          <div className="course-quantity-order">
            <span className="quantity-item">Quantity:</span>
            <button
              className="btn-minus"
              onClick={() => {
                if (quantity > 1) {
                  setQuantity(quantity - 1);
                } else {
                  setQuantity(1);
                }
              }}
            >
              <i className="icon-minus fas fa-minus-circle"></i>
            </button>
            <span className="quantity-existing">{quantity}</span>
            <button
              className="btn-plus"
              onClick={() => {
                setQuantity(quantity + 1);
              }}
            >
              <i className="icon-plus fas fa-plus-circle"></i>
            </button>
          </div>
          <div className="add-separate"></div>
          {/* phần tổng tiền */}
          <div className="detail-order-money">
            <span>
              Total:&nbsp;
              {product.productPrice ? product.productPrice * quantity : 0} $
            </span>
          </div>
          {/* Thêm sp vào giỏ hàng */}
          <div className="detail-btn-to-add">
            <button className="btn-add-to" onClick={handleBtnBuyClick}>
              Thêm vào giỏ hàng
            </button>
          </div>
          <div className="detail-btn-to-back">
            <button className="btn-back-to-home" onClick={backToHome}>
              Quay lại
            </button>
          </div>
        </div>
      </div>
      <div className="course-other">
        <CourseList />
      </div>
    </div>
  );
};

export default DetailCoures;
