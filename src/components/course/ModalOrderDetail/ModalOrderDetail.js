import React, { useState, useEffect } from "react";
import { Modal } from "react-bootstrap";
import axios from "axios";
import { Link, useHistory } from "react-router-dom";
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  Grid,
} from "@mui/material";

export const ModalOrderDetail = ({
  show,
  orderDetail,
  handlerClose,
  backHome,
}) => {
  const history = useHistory();

  // sét trạng thái các sản phẩm 1 mảng rỗng để gán lại giá trị sau khi gọi api
  const [course, setCourse] = useState([]);
  //hàm thực hiện gọi api lấy ra course
  useEffect(() => {
    const getListCourse = async () => {
      const listCourse = await axios.get("http://localhost:8000/products");
      setCourse(listCourse.data.Product);
    };
    getListCourse();
  }, []);

  const changeNameProduct = (productId) => {
    let result = [];
    result = course.filter((prod) => {
      return prod._id === productId;
    });
    return result[0].productName;
  };

  // đường dẫn vào detail của sản phẩm
  const detailUrl = "/detail/";

  // hàm thực hiện quay lại trang detail cho sản phẩm
  const handleBuyAgain = (e) => {
    history.push("/detail/" + e.productId);
  };

  return (
    <div className="modal-detail-order">
      <Modal size="xl" show={show} onHide={handlerClose}>
        <Modal.Header closeButton>
          <Modal.Title>Chi Tiết Đơn Hàng</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Grid container>
            <TableContainer component={Paper}>
              <Table sx={{ minWidth: 650 }} aria-label="simple table">
                <TableHead>
                  <TableRow>
                    <TableCell
                      align="center"
                      style={{ border: "1px solid #f7e7ce" }}
                    >
                      STT
                    </TableCell>
                    <TableCell
                      align="center"
                      style={{ border: "1px solid #f7e7ce" }}
                    >
                      OrderId
                    </TableCell>
                    <TableCell
                      align="center"
                      style={{ border: "1px solid #f7e7ce" }}
                    >
                      Product name
                    </TableCell>
                    <TableCell
                      align="center"
                      style={{ border: "1px solid #f7e7ce" }}
                    >
                      Quantity
                    </TableCell>
                    <TableCell
                      align="center"
                      style={{ border: "1px solid #f7e7ce" }}
                    >
                      Price
                    </TableCell>
                    <TableCell
                      align="center"
                      style={{ border: "1px solid #f7e7ce" }}
                    >
                      Action
                    </TableCell>
                  </TableRow>
                </TableHead>
                <TableBody style={{ border: "1px solid #f7e7ce" }}>
                  {orderDetail.map((element, index) => (
                    <TableRow
                      key={index}
                      sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                    >
                      <TableCell
                        component="th"
                        scope="row"
                        align="center"
                        style={{ border: "1px solid #f7e7ce" }}
                      >
                        {index + 1}
                      </TableCell>
                      <TableCell
                        align="center"
                        style={{ border: "1px solid #f7e7ce" }}
                      >
                        {element.orderId}
                      </TableCell>
                      <TableCell
                        style={{ border: "1px solid #f7e7ce" }}
                        align="center"
                      >
                        <Link
                          to={detailUrl + element.productId}
                          className="link-change-course"
                        >
                          {changeNameProduct(element.productId)}
                        </Link>
                      </TableCell>
                      <TableCell
                        style={{ border: "1px solid #f7e7ce" }}
                        align="center"
                      >
                        {element.quantity}
                      </TableCell>
                      <TableCell
                        style={{ border: "1px solid #f7e7ce" }}
                        align="center"
                      >
                        {element.priceEach}
                      </TableCell>

                      <TableCell align="center">
                        <button
                          className="btn-buy-back"
                          onClick={() => handleBuyAgain(element)}
                        >
                          Mua lại
                        </button>
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
          </Grid>
        </Modal.Body>
        <Modal.Footer>
          <button className="btn-close-modal-detail" onClick={handlerClose}>
            Đóng
          </button>
          <button className="btn-back-to-home-detail" onClick={backHome}>
            Quay lại
          </button>
        </Modal.Footer>
      </Modal>
      <div></div>
    </div>
  );
};

export default ModalOrderDetail;
