import React from "react";
import Header from "./header/Header.js";
import Content from "./content/Content.js";
import Notification from "./notification/Notification.js";
import CourseList from "./course/CourseList.js";
import Footer from "./footer/Footer.js";
export const HomePage = () => {
  return (
    <div>
      <Header />
      <Content />
      <Notification />
      <CourseList />
      <Footer />
    </div>
  );
};

export default HomePage;
