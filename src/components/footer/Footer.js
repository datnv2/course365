import React from "react";

export const Footer = () => {
  return (
    <div className="main">
      <div className="footer">
        <div className="grid">
          <div className="line-footer-separate"></div>
          <footer className="footer-list">
            <ul className="footer-list-item">
              <li className="footer-list-item-text all-rights-reserved ">
                © 2022 geeks. all rights reserved.
              </li>
            </ul>
            <ul className="footer-list-item">
              <li className="footer-list-item-text">
                <a className="footer-list-item-link" href="#">
                  Privacy
                </a>
              </li>
              <li className="footer-list-item-text">
                <a className="footer-list-item-link" href="#">
                  Terms
                </a>
              </li>
              <li className="footer-list-item-text">
                <a className="footer-list-item-link" href="#">
                  Feedback
                </a>
              </li>
              <li className="footer-list-item-text">
                <a className="footer-list-item-link" href="#">
                  Support
                </a>
              </li>
            </ul>
          </footer>
        </div>
      </div>
    </div>
  );
};
export default Footer;
