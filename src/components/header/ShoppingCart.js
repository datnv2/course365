import React, { useState, useEffect } from "react";
import axios from "axios";
import { useSelector, useDispatch } from "react-redux";
import { useHistory, Link } from "react-router-dom";
import {
  removeProduct,
  incrementProduct,
  decrementProduct,
  deleteAllProduct,
} from "../../actions/cart.js";
import Header from "./Header.js";
import Footer from "../footer/Footer.js";
import { Modal } from "react-bootstrap";
export const ShoppingCart = (props) => {
  const history = useHistory();
  const listProduct = useSelector((state) => state.cart.listproduct);
  const dispatch = useDispatch();

  // hàm này thực hiện việc xóa sản phẩm được click trong giỏ hàng
  const onDeleteCourse = (id) => {
    const action = removeProduct(id);
    dispatch(action);
  };

  // set id của order khi được tạo ban đầu là rỗng
  const [orderId, setOrderId] = useState("");
  // sét trạng thái ban đầu cho modal khi tạo đơn hàng thành công là đóng
  const [showCreated, setShowCreated] = useState(false);
  const handlerCloseModalCreated = () => {
    setShowCreated(false);
  };

  //trạng thái modal khi xóa sp khỏi giỏ hàng
  const [show, setShow] = useState(false);
  // set trạng thái của modal khi người dùng bấm tiếp tục mua hàng và show ra modal chi tiết
  //các sản phẩm và bao ngoài các modal thông báo khác
  const [showProduct, setShowProduct] = useState(false);

  // trạng thái để xác định đó là chi tiết đơn hàng hay là thông báo
  const [modalProductState, setModalProductState] = useState(false);

  // trạng thái để check xem người dùng xóa 1 sản phẩm hay xóa toàn bộ
  const [modalState, setModalState] = useState(true);

  //hành động thực thi khi người dùng xóa 1 sp
  const [productEnd, setProductEnd] = useState();

  // hàm thực hiện đóng modal khi xóa sp khỏi giỏ hàng
  const handlerClose = () => {
    setShow(false);
  };

  // sét trạng thái trên input để người dùng viết ghi chú khi tạo đơn
  const [note, setNote] = useState("");

  // hàm thực hiện đóng modal tạo đơn hàng và quay lại trang chủ
  const handleCloseProduct = () => {
    setShowProduct(false);
    history.push("/");
  };

  //thực hiện check giỏ hàng nếu user chưa đăng nhập thì điều hướng về trang login
  useEffect(() => {
    if (localStorage.name === null || localStorage.name === undefined) {
      history.push("/login");
    }
  });
  //khai báo kết quả của giỏ hàng là 1 mảng các sản phẩm
  let result = [];
  //thêm các phẩm cùng với giá và số lượng vào mảng sản phẩm
  listProduct.map((data) =>
    result.push(data.product.productPrice * data.product.productQuantity)
  );

  // thực hiện tính tiền khi mua sp
  const totalPrice = result.reduce(function (acc, value) {
    return acc + value;
  }, 0);

  // hàm thực hiện để giảm số lượng sp trong giỏ hàng
  const onDecrementClick = (product) => {
    if (product.product.productQuantity > 1) {
      const action = decrementProduct(product);
      dispatch(action);
    } else {
      setShow(true);
      setProductEnd(product.idPro);
    }
  };

  // hàm để thực hiện tăng số lượng sp trong giỏ hàng
  const onIncrementClick = (product) => {
    const action = incrementProduct(product);
    dispatch(action);
  };

  // hàm này để thực hiện việc user xóa 1 hay nhiều sp
  const handleRemoveEndItem = () => {
    if (modalState) {
      // thực hiện xóa 1 sp
      const action = removeProduct(productEnd);
      dispatch(action);
      setShow(false);
    }
    //xóa toàn bộ
    else {
      const action = deleteAllProduct(listProduct);
      dispatch(action);
      setShow(false);
    }
  };

  // hàm thực hiện xóa toàn bộ sp
  const onDeleteAll = () => {
    if (listProduct.length > 0) {
      setShow(true);
      setModalState(false);
      setModalProductState(false);
    } else {
      return;
    }
  };

  // hàm này thực hiện việc khi click vào tiếp tục mua hàng thì hiển thị lên danh sách các sp đã chọn mua
  const onBuyNowClick = () => {
    if (listProduct.length === 0) {
      setShowProduct(true);
    } else {
      setModalProductState(true);
      setShowProduct(true);
    }
  };

  // khai báo biến ngày tháng năm
  let today = new Date();
  let date =
    today.getDate() +
    "/" +
    parseInt(today.getMonth() + 1) +
    "/" +
    today.getFullYear();

  // hàm này thực hiện gọi api để tạo đơn hàng
  const onBtnCreateOrderClick = async () => {
    // thông tin đơn hàng
    const orderObj = {
      customerId: localStorage.id,
      orderDate: today,
      requiredDate: today,
      shippedDate: today,
      note: note,
    };

    // gọi api tạo đơn hàng mới
    const createOrder = await axios.post(
      "http://localhost:8000/order",
      orderObj
    );
    if (createOrder.data.success) {
      setOrderId(createOrder.data.Order._id);
      setShowProduct(false);
      setShowCreated(true);
    }
  };

  // hàm này thực hiện việc check chi tiết đơn hàng
  const onBtnSuccessCreateOrder = async () => {
    for (var bI = 0; bI < listProduct.length; bI++) {
      const orderDetailObject = {
        orderId: orderId,
        productId: listProduct[bI].product._id,
        quantity: listProduct[bI].product.productQuantity,
        priceEach: listProduct[bI].product.productPrice,
      };
      //gọi api để tạo chi tiết đơn hàng
      const orderDetail = await axios.post(
        "http://localhost:8000/order-detail",
        orderDetailObject
      );
      if (orderDetail.data.success) {
        setShowCreated(false);
        history.push("/");
      }
    }
  };
  return (
    <div>
      <Header />
      <div className="cart-container">
        <div className="cart-notifi-warp">
          <h4 className="cart-notifi-text">List Course</h4>
          <div className="cart-info-header">
            <div className="cart-info-header-name">Tên khóa học</div>
            <div className="cart-info-header-price">Giá khóa học</div>
            <div className="cart-info-header-quantity">Số lượng</div>
            <div className="cart-info-header-total">Thành tiền</div>
            <div className="cart-info-header-delete">
              <i className="icon-trash fas fa-trash" onClick={onDeleteAll}></i>
            </div>
          </div>

          {/* map sản phẩm */}
          <div className="cart-content">
            {listProduct.map((element, index) => (
              <div key={index} className="cart-content-header">
                <div className="cart-content-header-name">
                  {element.product.productName}
                </div>
                <div className="cart-content-header-price">
                  {element.product.productPrice}$
                </div>
                <div className="cart-content-header-quantity">
                  <button className="icon-remove">
                    <span
                      className="icon-remove-minus fas fa-minus-circle"
                      onClick={() => onDecrementClick(element)}
                    />
                  </button>
                  <span className="product-quantity-exist">
                    {element.product.productQuantity}
                  </span>
                  <button className="icon-add">
                    <span
                      class="icon-add-plus fas fa-plus-circle"
                      onClick={() => onIncrementClick(element)}
                    />
                  </button>
                </div>
                <div className="cart-content-header-money">
                  {element.product.productPrice *
                    element.product.productQuantity}
                  $
                </div>
                <div className="cart-content-header-delete">
                  <span
                    className="icon-delete-trash fas fa-trash"
                    onClick={() => onDeleteCourse(element.idPro)}
                  />
                </div>
              </div>
            ))}
          </div>
        </div>
        {/* Thông tin giao hàng */}
        <div className="user-notifications-adress">
          <div className="user-adress-local">
            <div className="user-noti-info">
              <div>
                Thông tin giao hàng
                <i className="user-icon-truck fas fa-truck" />
              </div>
              <Link to="/customer-info" className="user-change-address-link">
                <div className="user-change">Thay đổi địa chỉ</div>
              </Link>
            </div>
            {/* load thông tin của người dùng trên localStorage ra */}
            <div className="user-info-name-header">
              <span className="localStorage-name">
                Họ tên: {localStorage.name}
              </span>
            </div>
            <div className="user-info-phone">
              <span className="localStorage-phone">
                Số điện thoại: {localStorage.phone}
              </span>
            </div>
            <div className="user-info-address">
              <span className="localStorage-address">
                Địa chỉ: {localStorage.address}
              </span>
            </div>
          </div>
          {/* Tổng số sản phẩm và giá tiền */}
          <div className="user-info-quantity-detail">
            <div className="user-info-quantity--number">
              <div className="user-info-quantity-total">Tổng số sản phẩm: </div>
              <div className="user-info-quantity-length">
                {listProduct.length}
              </div>
            </div>
            <div className="user-info-total-money">
              <div className="user-info-total--number-money">Tổng tiền:</div>
              <div className="user-info-calculated">{totalPrice}$</div>
            </div>
          </div>
          <button className="btn-continue-buy-now" onClick={onBuyNowClick}>
            Tiếp tục mua hàng
          </button>
        </div>
      </div>
      <div className="wrarper-footer">
        <Footer />
      </div>
      {/* Modal để ktra xem xóa 1 sản phẩm hay xóa toàn bộ sản phẩm */}
      <Modal show={show} onHide={handlerClose} animation={true}>
        <Modal.Header closeButton>
          <Modal.Title>Thông báo</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {modalState
            ? "Bạn có muốn xóa khóa học này không"
            : "Bạn có muốn xóa toàn bộ khóa học không"}
        </Modal.Body>
        <Modal.Footer>
          <button className="btn-close-modal" onClick={handlerClose}>
            Đóng
          </button>
          <button className="btn-remove" onClick={handleRemoveEndItem}>
            Xóa
          </button>
        </Modal.Footer>
      </Modal>

      {/* Modal Chi tiết đơn hàng */}
      <Modal
        size={modalProductState ? "lg" : "md"}
        show={showProduct}
        onHide={handleCloseProduct}
        animation={true}
      >
        <Modal.Header closeButton>
          <Modal.Title>
            {modalProductState ? "Chi tiết đơn hàng" : "Thông báo"}
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {modalProductState ? (
            <div className="modal-content-order">
              <div className="content-product">
                {listProduct.map((element, index) => (
                  <div key={index} className="content-product-body">
                    <div className="content-product-img">
                      <img
                        className="content-product-img-detail"
                        alt=""
                        src={element.product.productImage[0]}
                      />
                    </div>
                    <div className="content-product-name">
                      {element.product.productName}
                    </div>
                    <div className="content-product-quantity">
                      {element.product.productQuantity}
                    </div>
                    <div className="content-product-money">
                      {element.product.productPrice *
                        element.product.productQuantity}{" "}
                      {""}$
                    </div>
                  </div>
                ))}
              </div>
              {/* Thông tin user */}
              <div className="content-user">
                <div className="info-user">
                  <label className="info-user-name">Full name: </label>
                  <p className="info-user-local-name">{localStorage.name}</p>
                </div>
                <div className="info-order">
                  <label className="info-order-address">Address:</label>
                  <p className="info-order-local-address">
                    {localStorage.address}
                  </p>
                </div>
                <div className="info-phone">
                  <label className="info-phone-number">Phone number:</label>
                  <p className="info-phone--local-number">
                    {localStorage.phone}
                  </p>
                </div>
                <div className="info-email">
                  <label className="info-email-user-order">Email:</label>
                  <p className="info-email-local-user-order">
                    {localStorage.email}
                  </p>
                </div>

                <div className="info-note-noti">
                  <label className="info-text-note">Note:</label>
                  <textarea
                    onChange={(e) => {
                      setNote(e.target.value);
                    }}
                    className="info-note"
                  ></textarea>
                </div>
                <div className="info-shipper">
                  <label className="info-shipped">Shipped date:</label>
                  <p className="info-shipped-real-time">{date}</p>
                </div>
              </div>
            </div>
          ) : (
            "Vui lòng chọn khóa học phù hợp để tạo order"
          )}
        </Modal.Body>
        <Modal.Footer>
          {modalProductState ? (
            <button className="btn-cancel-order" onClick={handleCloseProduct}>
              Đóng
            </button>
          ) : (
            ""
          )}
          <button
            className="btn-update-order"
            onClick={
              modalProductState ? onBtnCreateOrderClick : handleCloseProduct
            }
          >
            {modalProductState ? "Tạo đơn" : "Cảm ơn"}
          </button>
        </Modal.Footer>
      </Modal>

      {/* modal created order */}
      <Modal
        show={showCreated}
        onHide={handlerCloseModalCreated}
        backdrop="static"
        keyboard={false}
      >
        <Modal.Header closeButton>
          <Modal.Title>Thông báo</Modal.Title>
        </Modal.Header>
        <Modal.Body>Đã tạo đơn hàng thành công</Modal.Body>
        <Modal.Footer>
          <button className="btn-update" onClick={onBtnSuccessCreateOrder}>
            Đóng
          </button>
        </Modal.Footer>
      </Modal>
    </div>
  );
};

export default ShoppingCart;
