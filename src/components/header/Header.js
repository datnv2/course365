import React from "react";
import "firebase/compat/auth";
import logo from "../../assets/logo.png";
import avarta from "../../assets/avatar.jpg";
import { signOutWithGoogle } from "../../Firebase/Config.js";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import SearchProduct from "./SearchProduct.js";

export const Header = () => {
  const userName = localStorage.getItem("name");
  const emailDetail = localStorage.getItem("email");
  const listProduct = useSelector((state) => state.cart.listproduct);
  console.log(listProduct);

  //khai báo biến để hiển thị số lượng sp có trong giỏ hàng
  const itemProduct = listProduct.length;

  return (
    <div className="warp">
      <header className="header">
        <div className="grid">
          <nav className="header__narbar">
            <ul className="header__narbar-list">
              <li className="header__narbar-item">
                <Link to="/">
                  <img className="header__narbar-item-logo" src={logo} />
                </Link>
              </li>
              {/* Start  Browse */}
              <li className="header__narbar-item">
                <a
                  href=""
                  className="header__narbar-item-link header__narbar-item-browse"
                >
                  Browse
                  <i className="header__narbar-item-icon fas fa-angle-down"></i>
                </a>
                {/* thành phần con Browse là 1 list ul-li */}
                <ul className="header__narbar-course-list-item">
                  <li className="header__narbar-course-list-item-web">
                    <a href="#">
                      Web Development
                      <i className="header__narbar-course-list-item-icon fas fa-angle-right"></i>
                    </a>

                    {/* thẻ con của web development */}
                    <ul className="header__narbar-course-library">
                      <li>
                        <a href="#">bootstrap</a>
                      </li>
                      <li>
                        <a href="#">react</a>
                      </li>
                      <li>
                        <a href="#">graphQL</a>
                      </li>
                      <li>
                        <a href="#">gatsby</a>
                      </li>
                      <li>
                        <a href="#">Grunt</a>
                      </li>
                      <li>
                        <a href="#">svelte</a>
                      </li>
                      <li>
                        <a href="#">meteor</a>
                      </li>
                      <li>
                        <a href="#">html5</a>
                      </li>
                      <li>
                        <a href="#">angular</a>
                      </li>
                    </ul>
                  </li>
                  {/*Design  */}
                  <li className="header__narbar-course-list-item-design">
                    <a href="#">
                      Design
                      <i className="header__narbar-course-list-item-icon-design fas fa-angle-right"></i>
                    </a>

                    {/* thẻ ul con của design */}
                    <ul className="header__narbar-course-design-list">
                      <li>
                        <a href="#">graphic design</a>
                      </li>
                      <li>
                        <a href="#">illustrator</a>
                      </li>
                      <li>
                        <a href="#">UX/UI design</a>
                      </li>
                      <li>
                        <a href="#">figma design</a>
                      </li>
                      <li>
                        <a href="#">adobe XD</a>
                      </li>
                      <li>
                        <a href="#">sketch</a>
                      </li>
                      <li>
                        <a href="#">icon design</a>
                      </li>
                      <li>
                        <a href="#">photoshop</a>
                      </li>
                    </ul>
                  </li>
                  {/*Design  */}
                  <li className="header__narbar-course-list-item-mobile">
                    <a href="#">Mobile App</a>
                  </li>
                  <li className="header__narbar-course-list-item-software">
                    <a href="#">It Software</a>
                  </li>
                  <li className="header__narbar-course-list-item-marketing">
                    <a href="#">Marketing</a>
                  </li>
                  <li className="header__narbar-course-list-item-music">
                    <a href="#">Music</a>
                  </li>
                  <li className="header__narbar-course-list-item-life">
                    <a href="#">Life Style</a>
                  </li>
                  <li className="header__narbar-course-list-item-susiness">
                    <a href="#">Susiness</a>
                  </li>
                  <li className="header__narbar-course-list-item-photography">
                    <a href="#">Photography</a>
                  </li>
                </ul>
              </li>
              {/* End  Browse */}

              {/* Start Landing */}
              <li className="header__narbar-item">
                <a
                  href=""
                  className="header__narbar-item-link  header__narbar-item-landings"
                >
                  Landings
                  <i className="header__narbar-item-icon fas fa-angle-down"></i>
                </a>
                {/* thẻ ul con trong landing */}
                <ul className="header__narbar-landing">
                  <li className="header__narbar-landing-item">landings</li>
                  <li>
                    <a href="#">courses</a>
                  </li>
                  <li>
                    <a href="#">lead courses</a>
                  </li>
                  <li>
                    <a href="#">requests access</a>
                  </li>
                  <li>
                    <a href="#">sass</a>
                  </li>
                  <li className="header__narbar-landing-job-listing">
                    job listing
                    <button className="header__narbar-new-btn" href="#">
                      New
                    </button>
                  </li>
                </ul>
              </li>

              {/* End Landing */}

              {/* Start Pages  */}

              <li className="header__narbar-item ">
                <a
                  href=""
                  className="header__narbar-item-link  header__narbar-item-pages"
                >
                  Pages
                  <i className="header__narbar-item-icon fas fa-angle-down"></i>
                </a>
                {/* thẻ ul con trong Pages */}
                <ul className="header__narbar-item-pages-list">
                  {/* Start courses */}
                  <li className="header__narbar-parent-courses">
                    <a href="">
                      courses
                      <i className="header__narbar-item-icon-angle-right fas fa-angle-right"></i>
                    </a>
                    {/* thẻ ul con trong courses */}
                    <ul className="header__narbar-item-courses-version">
                      <li>
                        <a href="">courses single</a>
                      </li>
                      <li>
                        <a href="">courses single v2</a>
                      </li>
                      <li>
                        <a href=""> courses resume</a>
                      </li>
                      <li>
                        <a href=""> courses category</a>
                      </li>
                      <li>
                        <a href="">courses checkout</a>
                      </li>
                      <li>
                        <a href=""> courses list/grid</a>
                      </li>
                      <li>
                        <a href=""> add new courses</a>
                      </li>
                    </ul>
                  </li>

                  {/* End courses */}

                  {/* Start  paths */}
                  <li className="header__narbar-parent-paths">
                    <a href="">
                      paths
                      <i className="header__narbar-item-icon-angle-paths fas fa-angle-right"></i>
                    </a>
                    {/* thẻ ul con paths */}
                    <ul className="header__narbar-item-paths-version">
                      <li>
                        <a href=""> browser path</a>
                      </li>
                      <li>
                        <a href=""> path single</a>
                      </li>
                    </ul>
                  </li>
                  {/* End paths */}

                  {/* Start blog */}
                  <li className="header__narbar-parent-blog">
                    <a href="">
                      blog
                      <i className="header__narbar-item-icon-angle-blog fas fa-angle-right"></i>
                    </a>
                    {/* thẻ ul con bên trong blog */}
                    <ul className="header__narbar-item-blog-version">
                      <li>
                        <a href="">listing</a>
                      </li>
                      <li>
                        <a href="">article</a>
                      </li>
                      <li>
                        <a href="">category</a>
                      </li>
                      <li>
                        <a href="">sidebar</a>
                      </li>
                    </ul>
                  </li>
                  {/* End blog */}

                  {/* Start career */}
                  <li className="header__narbar-parent-career">
                    <a href="">
                      career
                      <i className="header__narbar-item-icon-angle-career fas fa-angle-right"></i>
                    </a>
                    {/* thẻ ul con bên trong career */}
                    <ul className="header__narbar-item-career-version">
                      <li>
                        <a href="">overview</a>
                      </li>
                      <li>
                        <a href=""> listing</a>
                      </li>
                      <li>
                        <a href="">opening</a>
                      </li>
                    </ul>
                  </li>
                  {/* End career */}

                  {/*Start portfolio  */}
                  <li className="header__narbar-item-pages-list-portfolio">
                    portfolio
                    <button
                      className="header__narbar-item-pages-btn-new"
                      href=""
                    >
                      new
                      <i className="header__narbar-item-icon-angle-portfolio fas fa-angle-right"></i>
                    </button>
                    {/* thẻ ul con bên trong portfolio */}
                    <ul className="header__narbar-item-portfolio-version">
                      <li>
                        <a href="">list</a>
                      </li>
                      <li>
                        <a href="">single</a>
                      </li>
                    </ul>
                  </li>
                  {/*End portfolio  */}

                  {/* Start job */}
                  <li className="header__narbar-item-pages-list-job">
                    job
                    <button
                      className="header__narbar-item-pages-btn-new"
                      href=""
                    >
                      new
                      <i className="header__narbar-item-icon-angle-job fas fa-angle-right"></i>
                    </button>
                    {/* thẻ ul con bên trong job */}
                    <ul className="header__narbar-item-job-version">
                      <li>
                        <a href="">home</a>
                      </li>
                      <li>
                        <a href="">list</a>
                      </li>
                      <li>
                        <a href="">gird</a>
                      </li>
                      <li>
                        <a href="">single</a>
                      </li>
                      <li>
                        <a href="">company list</a>
                      </li>
                      <li>
                        <a href="">company single</a>
                      </li>
                    </ul>
                  </li>
                  {/*End job  */}

                  {/* Start specialty */}
                  <li className="header__narbar-item-pages-list-specialty">
                    <a href="">
                      specialty
                      <i className="header__narbar-item-icon-angle-specialty fas fa-angle-right"></i>
                    </a>
                    {/* thẻ ul con bên trong specialty */}
                    <ul className="header__narbar-item-specialty-version">
                      <li>
                        <a href="">comming soon</a>
                      </li>
                      <li>
                        <a href="">error 404</a>
                      </li>
                      <li>
                        <a href="">maintenance mode</a>
                      </li>
                      <li>
                        <a href="">terms &amp; conditions</a>
                      </li>
                    </ul>
                  </li>
                  {/* End */}

                  <div className="header__narbar-border-bootom"></div>
                  <li>
                    <a href="">about</a>
                  </li>

                  {/* Start help center */}
                  <li className="header__narbar-item-pages-list-help-center">
                    <a href="">
                      help center
                      <i className="header__narbar-item-icon-angle-help-center fas fa-angle-right"></i>
                    </a>
                    {/* thẻ ul con bên trong help center */}
                    <ul className="header__narbar-item-help-center-version">
                      <li>
                        <a href="">help center</a>
                      </li>
                      <li>
                        <a href=""> FAQ's</a>
                      </li>
                      <li>
                        <a href=""> guide</a>
                      </li>
                      <li>
                        <a href="">guide single</a>
                      </li>
                      <li>
                        <a href="">support</a>
                      </li>
                    </ul>
                  </li>
                  {/* End help center */}

                  <li>
                    <a href="">pricing</a>
                  </li>
                  <li>
                    <a href="">conpare plan</a>
                  </li>
                  <li>
                    <a href="">contact</a>
                  </li>
                </ul>
              </li>
              {/* End Pages */}

              {/* Start Accounts */}
              <li className="header__narbar-item">
                <a
                  href=""
                  className="header__narbar-item-link  header__narbar-item-accounts"
                >
                  Accounts
                  <i className="header__narbar-item-icon fas fa-angle-down"></i>
                </a>
                {/* thẻ ul bên trong Accounts */}
                <ul className="accounts-list">
                  <li>
                    <span className="account-first">accounts</span>
                  </li>
                  {/* Start  instructor */}
                  <li className="instructor-parent">
                    <a href="#">
                      instructor
                      <i className="icon-instructor fas fa-angle-right"></i>
                    </a>
                    {/* thẻ ul bên trong instructor  */}
                    <ul className="instructor-children">
                      <li>
                        <span className="instructor-text">instructor</span>
                        <p className="dashboard">
                          Instructor dashboard for manage courses and earning.
                        </p>
                      </li>
                      <li>
                        <div className="dot-border"></div>
                      </li>
                      <li>
                        <a href="">dashboard</a>
                      </li>
                      <li>
                        <a href="">profile</a>
                      </li>
                      <li>
                        <a href="">my courses</a>
                      </li>
                      <li>
                        <a href="">orders</a>
                      </li>
                      <li>
                        <a href="">reviews</a>
                      </li>
                      <li>
                        <a href="">students</a>
                      </li>
                      <li>
                        <a href="">payouts</a>
                      </li>
                      <li>
                        <a className="earning" href="">
                          earning
                        </a>
                      </li>
                    </ul>
                  </li>
                  {/* End  instructor */}

                  {/* Start students */}
                  <li className="students-parent">
                    <a href="#">
                      students
                      <i className="icon-students fas fa-angle-right"></i>
                    </a>
                    {/* thẻ ul bên trong students */}
                    <ul className="students-child">
                      <li className="students-first">
                        <span className="students-text">students</span>
                        <p className="dashboard-manage">
                          Students dashboard to manage your courses and
                          subscriptions.
                        </p>
                      </li>
                      <li>
                        <div className="line-border-top"></div>
                      </li>
                      <li>
                        <a href=""> dashboard</a>
                      </li>
                      <li>
                        <a href="">subcriptions</a>
                      </li>
                      <li>
                        <a href="">payments</a>
                      </li>
                      <li>
                        <a href="">billing info</a>
                      </li>
                      <li>
                        <a href="">invoice</a>
                      </li>
                      <li>
                        <a href="">invoice details</a>
                      </li>
                      <li>
                        <a href="">bookmarked</a>
                      </li>
                      <li>
                        <a className="my-path" href="">
                          my path
                        </a>
                      </li>
                    </ul>
                  </li>
                  {/* End students */}

                  {/* Start admin */}
                  <li className="admin-parent">
                    <a href="#">
                      admin
                      <i className="icon-admin fas fa-angle-right"></i>
                    </a>
                    {/* thẻ ul bên trong admin */}
                    <ul className="admin-child">
                      <li className="admin-first">
                        <span className="master-text">master admin</span>
                        <p className="amazing-apps">
                          Master admin dashboard to manage courses, user, site
                          setting , and work with amazing apps.
                        </p>
                      </li>
                      <li>
                        <div className="admin-line-border"></div>
                      </li>
                      <li className="admin-last">
                        <button className="btn-dashboard">
                          Go to Dashboard
                        </button>
                      </li>
                    </ul>
                  </li>
                  {/* End admin */}

                  <div className="dot"></div>
                  <li>
                    <a href="#">
                      sign in
                      {/* <i className="fas fa-angle-right"></i> */}
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      sign up
                      {/* <i className="fas fa-angle-right"></i> */}
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      forgot password
                      {/* <i className="fas fa-angle-right"></i> */}
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      edit profile
                      {/* <i className="fas fa-angle-right"></i> */}
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      security
                      {/* <i className="fas fa-angle-right"></i> */}
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      social profiles
                      {/* <i className="fas fa-angle-right"></i> */}
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      notifications
                      {/* <i className="fas fa-angle-right"></i> */}
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      privacy settings
                      {/* <i className="fas fa-angle-right"></i> */}
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      delete profile
                      {/* <i className="fas fa-angle-right"></i> */}
                    </a>
                  </li>
                  <li>
                    <a href="#" className="linked-accounts">
                      linked accounts
                      {/* <i className="fas fa-angle-right"></i> */}
                    </a>
                  </li>
                </ul>
              </li>
              {/* End Accounts */}

              <li className="header__narbar-item">
                <a
                  href=""
                  className="header__narbar-item-link header__narbar-item-dot"
                >
                  ...
                </a>
                <ul className="dot-lists">
                  <li className="dot-lists-documentations">
                    <a href="" className="documentations-first">
                      <i className="icon-documentations far fa-file-alt"></i>
                      <span className="documentations-text">
                        documentations
                      </span>
                      <p className="all-text">Browse the all documentation</p>
                    </a>
                  </li>
                  <li className="dot-lists-changelog">
                    <a href="">
                      <i className="icon-change fas fa-layer-group"></i>
                      <span className="changelog-text">
                        Changelog
                        <span className="changelog-number-version">v2.4.0</span>
                      </span>
                      <p className="see-new">See what's new</p>
                    </a>
                  </li>
                </ul>
              </li>

              <li className="header__narbar-item ">
                <SearchProduct />
              </li>
            </ul>
            {/* Start Bell */}
            <ul className="header__narbar-list">
              <li className="header__narbar-item">
                <a className="shopping-cart-link" href="">
                  <Link to="/shopping-cart">
                    <i className="icon-cart fas fa-shopping-cart"></i>
                    <span className="shopping-cart-noti">{itemProduct}</span>
                  </Link>
                </a>
              </li>
              <li className="header__narbar-item header header__narbar-item-list">
                <a href="" className="header__narbar-item-notify">
                  <i className="header__narbar-item-notify-icon far fa-bell"></i>
                  <div className="header__narbar-item-notify-status"></div>
                </a>
                {/* thẻ ul con trong bell */}
                <ul className="bell-list">
                  <li className="header__notify-item-notifications">
                    <a className="notifications-text">
                      Notifications
                      <i className="icon-notifications fas fa-cog"></i>
                    </a>
                  </li>
                  <li className="header__notify-item header__notify-item--view">
                    <a href="" className="header__notify-link">
                      <img src={avarta} alt="" className="header__notify-img" />
                      <div className="header__notify-info">
                        <span className="header__notify-name">
                          Kristin Watson:
                          <span className="status-now"></span>
                        </span>
                        <span className="header__notify-description">
                          <span>
                            Krisitn Watsan like your
                            <i className="icon-times fas fa-times"></i>
                          </span>
                          <br />
                          comment on course Javascript Introduction!
                        </span>
                        <div className="thumbs-up-like">
                          <a className="house-ago">
                            <i className="icon-up far fa-thumbs-up"></i>2 hours
                            ago,
                          </a>
                          <span className="hour">2:19 PM</span>
                        </div>
                      </div>
                    </a>
                  </li>

                  <li className="header__notify-item header__notify-item--view">
                    <a href="" className="header__notify-link">
                      <img src={avarta} alt="" className="header__notify-img" />
                      <div className="header__notify-info">
                        <span className="header__notify-name">
                          Kristin Watson:
                          <span className="status-now"></span>
                        </span>
                        <span className="header__notify-description">
                          <span>
                            Krisitn Watsan like your
                            <i className="icon-times fas fa-times"></i>
                          </span>
                          <br />
                          comment on course Javascript Introduction!
                        </span>
                        <div className="thumbs-up-like">
                          <a className="house-ago">
                            <i className="icon-up far fa-thumbs-up"></i>2 hours
                            ago,
                          </a>
                          <span className="hour">2:19 PM</span>
                        </div>
                      </div>
                    </a>
                  </li>
                </ul>
              </li>

              {/*  End Bell  */}
              {/* Start avarta */}
              <li className="header__narbar-item header__narbar-item-list">
                <div className="header__narbar-parent">
                  <img
                    className="header__narbar-item-codescandy"
                    src={avarta}
                    alt=""
                  />
                  <div className="header-user-codescandy-status"></div>
                </div>
                {/* thẻ ul trong ảnh */}
                <ul className="header__narbar-item-user-list">
                  <li className="header__narbar-item-hover-list">
                    <img className="avarta-img" src={avarta} />
                    <div className="dot-active"></div>
                    <div className="user-info-detail">
                      <span className="user-info-name">{userName}</span>
                      <span className="user-info-email">{emailDetail}</span>
                    </div>
                  </li>
                  <li>
                    <div className="dot-border-info-user"></div>
                  </li>
                  {/* Start status */}
                  <li className="status-list-parent">
                    <a href="#" className="status-first">
                      <i className="icon-status far fa-circle"></i>
                      status
                      <i className="icon-arrown-right fas fa-angle-right"></i>
                    </a>
                    {/* thẻ ul con bên trong status */}
                    <ul className="list-status-active">
                      <li>
                        <a href="#" className="online-parent">
                          <div className="icon-online"></div>
                          online
                        </a>
                      </li>
                      <li>
                        <a href="#" className="offline-parent">
                          <div className="icon-offline"></div>
                          offline
                        </a>
                      </li>
                      <li>
                        <a href="#" className="away-parent">
                          <div className="icon-away"></div>
                          away
                        </a>
                      </li>
                      <li className="busy-bootom">
                        <a href="#" className="busy-parent">
                          <div className="icon-busy"></div>
                          busy
                        </a>
                      </li>
                    </ul>
                  </li>
                  {/* End status /my-order */}

                  <li>
                    <a href="/customer-info" className="profile">
                      <i className="icon-profile far fa-user"></i>
                      profile
                    </a>
                  </li>
                  <li>
                    <a href="/login" className="login">
                      <i className="icon-user-circle fas fa-user-circle"></i>
                      login
                    </a>
                  </li>

                  <li>
                    <a href="/create-account" className="create">
                      <i className="icon-create fas fa-user-plus"></i>
                      create account
                    </a>
                  </li>
                  <li>
                    <a href="#" className="create">
                      <Link to="/my-order">
                        <i className="icon-first-order fab fa-first-order"></i>
                        <span className="list-order-user-details">
                          {" "}
                          list order
                        </span>
                      </Link>
                    </a>
                  </li>
                  <li>
                    <a href="#" className="subcriptions">
                      <i className="icon-subcriptions far fa-star"></i>
                      <span className="list-order-user-subcriptions">
                        subcriptions
                      </span>
                    </a>
                  </li>
                  <li>
                    <a href="#" className="settings">
                      <i className="icon-settings fas fa-cog"></i>
                      <span className="list-order-user-settings">settings</span>
                    </a>
                  </li>
                  <li>
                    <div className="seperate"></div>
                  </li>
                  <li className="sign-out">
                    <a
                      href="#"
                      className="sign-out-click"
                      onClick={signOutWithGoogle}
                    >
                      <i className="icon-sign-out fas fa-power-off"></i>
                      sign out
                    </a>
                  </li>
                </ul>
              </li>
              {/* End avarta */}
              <li className="header__narbar-item">
                <a href="">
                  <i className="menu-icon fas fa-bars"></i>
                </a>
              </li>
            </ul>
          </nav>
        </div>
      </header>
    </div>
  );
};

export default Header;
