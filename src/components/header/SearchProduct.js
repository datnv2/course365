import React, { useState } from "react";
import { useHistory } from "react-router-dom";

export const SearchProduct = () => {
  const history = useHistory();
  const [inputSeach, setInputSeach] = useState("all");
  const [selectSearch, setSelectSearch] = useState("all");

  // hàm thực hiện tìm kiếm
  const searchBtn = () => {
    history.push("/products/" + selectSearch + "/" + inputSeach);
  };

  // hàm ghi nhận sự thay đổi trên input
  const changeValueInput = (e) => {
    if (e.target.value === "") {
      setInputSeach("all");
    } else {
      setInputSeach(e.target.value);
    }
  };
  return (
    <div className="warp-btn-search">
      <button className="header__search-btn">
        <i
          className="header__search-btn-icon fas fa-search"
          onClick={searchBtn}
        ></i>
      </button>
      <div className="header__search-input-warp">
        <input
          type="text"
          className="form-input-filter"
          onChange={(e) => changeValueInput(e)}
        />

        <select
          className="header__search-input"
          onChange={(e) => setSelectSearch(e.target.value)}
        >
          <option value="all">All</option>
          <option value="name">name</option>
        </select>
      </div>
    </div>
  );
};

export default SearchProduct;
