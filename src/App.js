import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import { BrowserRouter } from "react-router-dom";
import HomePageRouter from "./HomePageRouter";

function App() {
  return (
    <BrowserRouter>
      <HomePageRouter />
    </BrowserRouter>
  );
}

export default App;
