import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import HomePage from "./components/HomePage.js";
import DetailCoures from "./components/course/DetailCoures.js";
import Login from "./components/course/Login.js";
import CreateAccount from "./components/course/CreateAccount.js";
import CustomerInfo from "./components/course/CustomerInfo.js";
import ShoppingCart from "./components/header/ShoppingCart.js";
import CourseList from "./components/course/CourseList.js";
import MyOrder from "./components/course/MyOrder.js";
export const HomePageRouter = () => {
  return (
    <Router>
      <Switch>
        <Route exact path="/" component={HomePage} />
        <Route exact path="/detail/:id" component={DetailCoures} />
        <Route exact path="/login" component={Login} />
        <Route exact path="/create-account" component={CreateAccount} />
        <Route exact path="/customer-info" component={CustomerInfo} />
        <Route exact path="/shopping-cart" component={ShoppingCart} />
        <Route exact path="/products/:key/:name" component={CourseList} />
        <Route exact path="/my-order" component={MyOrder} />
      </Switch>
    </Router>
  );
};
export default HomePageRouter;
