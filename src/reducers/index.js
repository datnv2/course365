import cartReducer from "./cart";
import { combineReducers } from "redux";
import useReducer from "./user";
const rootReducer = combineReducers({
  cart: cartReducer,
  user: useReducer,
});
export default rootReducer;
