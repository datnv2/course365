import { initializeApp } from "firebase/app";
import axios from "axios";
import {
  getAuth,
  GoogleAuthProvider,
  signInWithPopup,
  signOut,
} from "firebase/auth";

// phần key để kết nối với firebase khi đăng nhập
const firebaseConfig = {
  apiKey: "AIzaSyDSvfY5pTpN6SgsLA7eo9bLWlnm4jdar2s",
  authDomain: "fir-332c4.firebaseapp.com",
  projectId: "fir-332c4",
  storageBucket: "fir-332c4.appspot.com",
  messagingSenderId: "764080791961",
  appId: "1:764080791961:web:05a0bd1d1937910d2c3308",
  measurementId: "G-B8DJVJ5X3R",
};

const app = initializeApp(firebaseConfig);
export const auth = getAuth(app);
export const authOut = getAuth();
const provider = new GoogleAuthProvider();

//   thực hiện đăng nhập firebase với google
export const signInWithGoogle = () => {
  signInWithPopup(auth, provider)
    .then(async (result) => {
      const emailFill = await axios.get(
        "http://localhost:8000/customer-email?email=" + result.user.email
      );
      if (emailFill.data.customer) {
        const name = emailFill.data.customer.fullName;
        const id = emailFill.data.customer._id;
        const email = emailFill.data.customer.email;
        const profilePic = emailFill.data.customer.photoURL;
        const phone = emailFill.data.customer.phone;
        const adress = emailFill.data.customer.address;
        localStorage.setItem("name", name);
        localStorage.setItem("id", id);
        localStorage.setItem("email", email);
        localStorage.setItem("profilePic", profilePic);
        localStorage.setItem("phone", phone);
        localStorage.setItem("adress", adress);
        window.location.replace("http://localhost:3000");
      } else {
        const name = result.user.displayName;
        const email = result.user.email;
        localStorage.setItem("name", name);
        localStorage.setItem("email", email);
        window.location.replace("http://localhost:3000/create-account");
      }
    })
    .catch((error) => {
      console.error(error);
    });
};

// hàm thực hiện việc đăng xuất
export const signOutWithGoogle = () => {
  signOut(authOut)
    .then(() => {
      localStorage.removeItem("name");
      localStorage.removeItem("email");
      localStorage.removeItem("profilePic");
      window.location.replace("http://localhost:3000");
    })
    .catch((error) => {});
};
